package pe.edu.tecsup.vitamarket.repository

import pe.edu.tecsup.vitamarket.network.AuthApi
import pe.edu.tecsup.vitamarket.network.UserApi
import javax.inject.Inject

class UserRepository @Inject constructor(
    private val api: UserApi
) : BaseRepository(api){

    suspend fun getUser() = safeApiCall { api.getUser() }
}


