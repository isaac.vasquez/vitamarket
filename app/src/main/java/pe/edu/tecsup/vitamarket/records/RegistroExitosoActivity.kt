package pe.edu.tecsup.vitamarket.records

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import pe.edu.tecsup.vitamarket.R
import pe.edu.tecsup.vitamarket.login.Login

class RegistroExitosoActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_registro_exitoso)

        findViewById<Button>(R.id.button2).setOnClickListener{
            startActivity(Intent(this, Login::class.java))
        }
    }
}