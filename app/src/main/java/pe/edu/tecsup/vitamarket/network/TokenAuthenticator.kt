package pe.edu.tecsup.vitamarket.network

import android.content.Context
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.runBlocking
import okhttp3.Request
import okhttp3.Response
import okhttp3.Route
import pe.edu.tecsup.vitamarket.UserPreferences
import pe.edu.tecsup.vitamarket.repository.BaseRepository
import pe.edu.tecsup.vitamarket.responses.TokenResponse
import java.net.Authenticator
import javax.inject.Inject

class TokenAuthenticator @Inject constructor(
    context: Context,
    private val tokenApi: TokenRefreshApi
) : Authenticator, BaseRepository(tokenApi), okhttp3.Authenticator {

    private val appContext = context.applicationContext
    private val userPreferences = UserPreferences(appContext)

    override fun authenticate(route: Route?, response: Response): Request?{
        return runBlocking{
            when(val tokenResponse = getUpdateToken()){
                is Resource.Success -> {
                    userPreferences.saveAccessTokens(
                        tokenResponse.value.access_token!!,
                        tokenResponse.value.refresh_token!!
                    )
                    response.request.newBuilder()
                        .header("Authorization", "Bearer ${tokenResponse.value.access_token}")
                        .build()
                }
                else -> null
            }
        }
    }
    private suspend fun getUpdateToken(): Resource<TokenResponse>{
        val refreshToken = userPreferences.refreshToken.first()
        return safeApiCall{tokenApi.refreshAccessToken(refreshToken)}
    }
}