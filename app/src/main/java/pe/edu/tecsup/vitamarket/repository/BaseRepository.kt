package pe.edu.tecsup.vitamarket.repository

import pe.edu.tecsup.vitamarket.network.BaseApi
import pe.edu.tecsup.vitamarket.network.SafeApiCall

abstract class BaseRepository(private val api: BaseApi) : SafeApiCall {

    suspend fun logout() = safeApiCall {
        api.logout()
    }
}