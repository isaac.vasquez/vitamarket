package pe.edu.tecsup.vitamarket.network

import pe.edu.tecsup.vitamarket.responses.LoginResponse
import retrofit2.http.GET

interface UserApi : BaseApi {
    @GET("user")
    suspend fun getUser(): LoginResponse
}