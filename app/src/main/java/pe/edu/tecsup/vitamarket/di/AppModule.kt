package pe.edu.tecsup.vitamarket.di

import android.content.Context
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import pe.edu.tecsup.vitamarket.network.AuthApi
import pe.edu.tecsup.vitamarket.network.RemoteDataSource
import pe.edu.tecsup.vitamarket.network.UserApi
import javax.inject.Singleton


@Module
@InstallIn(SingletonComponent::class)
object AppModule {

    @Singleton
    @Provides
    fun provideAuthApi(
        remoteDataSource: RemoteDataSource,
        @ApplicationContext context:Context
    ): AuthApi{
        return remoteDataSource.buildApi(AuthApi::class.java, context)
    }

    @Singleton
    @Provides
    fun providesUserApi(
        remoteDataSource: RemoteDataSource,
        @ApplicationContext context: Context
    ): UserApi{
        return remoteDataSource.buildApi(UserApi::class.java,context)
    }
}