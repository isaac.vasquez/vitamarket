package pe.edu.tecsup.vitamarket.responses

data class LoginResponse (
    val user: User
)