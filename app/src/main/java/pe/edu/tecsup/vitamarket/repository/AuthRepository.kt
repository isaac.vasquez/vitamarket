package pe.edu.tecsup.vitamarket.repository

import pe.edu.tecsup.vitamarket.UserPreferences
import pe.edu.tecsup.vitamarket.network.AuthApi
import javax.inject.Inject

class AuthRepository @Inject constructor(
    private val api: AuthApi,
    private val preferences: UserPreferences
) : BaseRepository(api){

    suspend fun login(
        user: String,
        password: String
    ) = safeApiCall{
        api.login(user, password)
    }

    suspend fun saveAccessTokens(accessToken: String, refreshToken: String){
        preferences.saveAccessTokens(accessToken,refreshToken)
    }
}
