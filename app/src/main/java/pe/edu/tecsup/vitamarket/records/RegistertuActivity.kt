package pe.edu.tecsup.vitamarket.records

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import pe.edu.tecsup.vitamarket.R

class RegistertuActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_registertwo)

        findViewById<Button>(R.id.button).setOnClickListener{
            startActivity(Intent(this, RegistroExitosoActivity::class.java))
        }
    }
}