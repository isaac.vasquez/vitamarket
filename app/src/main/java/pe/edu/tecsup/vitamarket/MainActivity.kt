package pe.edu.tecsup.vitamarket

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import pe.edu.tecsup.vitamarket.records.RegistertuActivity

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        findViewById<Button>(R.id.buttonv).setOnClickListener{
            startActivity(Intent(this, Onboarding::class.java))
        }
    }
}