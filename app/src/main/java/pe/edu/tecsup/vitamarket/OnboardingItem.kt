package pe.edu.tecsup.vitamarket

data class OnboardingItem(
    val onboardingImage: Int,
    val title: String,
    val description: String
)