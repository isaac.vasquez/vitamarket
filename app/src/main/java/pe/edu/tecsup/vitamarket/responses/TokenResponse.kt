package pe.edu.tecsup.vitamarket.responses

data class TokenResponse(
    val access_token: String?,
    val refresh_token: String?
)