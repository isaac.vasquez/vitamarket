package pe.edu.tecsup.vitamarket.records

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import androidx.navigation.Navigator
import pe.edu.tecsup.vitamarket.R
import java.util.jar.Attributes

class RegisterActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_registerone)

        findViewById<Button>(R.id.Siguienteregister).setOnClickListener{
            startActivity(Intent(this, RegistertuActivity::class.java))
        }
    }
}